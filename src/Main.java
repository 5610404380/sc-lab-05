import model.Instructormodel;
import model.Staff;
import model.Student;
import model.Bookmodel;
import control.ControlBook;
import control.ControlInstructor;
import control.ControlRef;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Library lib = new Library();
		
		Student student = new Student("Mark","Rittikhun");
		ControlInstructor instructor = new ControlInstructor("Usa","Sammapan");
		Staff Staff = new Staff("Gaew","Lastname");
		
		ControlBook book1 = new ControlBook("Big JAVA","Watson");
		ControlBook book2 = new ControlBook("PPL","Ajarn");
		ControlRef refBook = new ControlRef("ABC","John");
		
		lib.addBook(book1);
		lib.addBook(book2);
		lib.addRefBook(refBook);
		
		System.out.println(lib.bookCount());
		lib.borrowBook(student, "Big JAVA");
		
		System.out.println(lib.getBorrowBook(book1));
		System.out.println(student.getName());

	}

}
