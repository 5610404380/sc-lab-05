package model;

public class Bookmodel {
	private String book;
	private String name;
	
	public Bookmodel(String book,String name){
		this.book = book;
		this.name = name;
	}
	
	public String getbook(){
		return book+" "+name;
	}
	
	public void setbook(String book){
		this.book = book;
	}
	
	
	public void setname(String name){
		this.name = name;
	}

}
