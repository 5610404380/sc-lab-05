package control;

import model.Bookmodel;

public class ControlBook {
	private String book;
	private String name;
	private Bookmodel bookname;

	public ControlBook(String book,String name){
		this.book = book;
		this.name= name;
	}
	
	public void setBook(){
		bookname.setbook(book);
	}
	
	public void setname(){
		bookname.setname(name);
	}
	
	public String getBook(){
		return bookname.getbook();
	}
	
	public String getbook(){
		return book+name;
	}

}
