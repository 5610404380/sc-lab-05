package control;

import model.Bookmodel;

public class ControlRef {
	private String ref;
	private String name;
	private Bookmodel bookname;
	
	
		public ControlRef(String ref ,String name){
			this.ref = ref;
			this.name= name;
		}
		
		public String getbook(){
			return ref;
		}
		
		public void setbook(String ref){
			this.ref = ref;
		}
		
		public String getname(){
			return name;
		}
		
		public void setname(String name){
			this.name = name;
		}

	}
