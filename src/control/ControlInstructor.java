package control;

import model.Instructormodel;

public class ControlInstructor {
	private String firstName;
	private String lastName;
	private Instructormodel inname;

	public ControlInstructor(String firstName , String lastName){
		this.firstName = firstName;
		this.lastName= lastName;
	}
	
	public void setinname(){
		inname.setinname(firstName,lastName);
	}
	
	public String getBookname(){
		return inname.getName();
	}


}
